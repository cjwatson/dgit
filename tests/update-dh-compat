#!/bin/bash
# dgit
# Integration between git and Debian-style archives
#
# Copyright (C)2013-2021 Ian Jackson
# Copyright (C)2017-2019 Sean Whitton
# Copyright (C)2019      Matthew Vernon / Genome Research Limited
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

# This ad-hoc script was wrtten to deal with #1002927.

export FILTER_BRANCH_SQUELCH_WARNING=1
toplevel=$PWD

cd_fresh_d () {
    cd "$toplevel"
    rm -rf d
    mkdir d
    cd d
}

adjust="$toplevel"/tests/update-dh-compat-adjust
adjust () { "$adjust"; }

git_filter_strip_origs () {
    git for-each-ref refs/original \
	--format '%(refname)' |
	xargs -rn1 git update-ref -d
}

adjust_git_filter () {
    # this is the dgit origin commit; this check may stop
    # us rewriting our normal dgit tree
    set +e
    git cat-file -e 06fb75074c0bebccdcf86385b8d248fd127ed66d
    rc=$?
    set -e
    test $rc = 1

    git_filter_strip_origs

    git filter-branch					\
	--tree-filter "$adjust"				\
	--tag-name-filter cat				\
	-- --all

    git_filter_strip_origs

    git gc --aggressive --prune=all
}

cd "$toplevel"

for f in tests/pkg-srcs/*.dsc; do
    cd_fresh_d

    dpkg-source -x ../$f p
    cd p
    adjust
    dpkg-source -b .
    cd ..
    dcmd rm ../$f
    dcmd mv *.dsc ../tests/pkg-srcs/

    cd ..
done

cd "$toplevel"

for f in tests/git-srcs/*.tar; do
    cd_fresh_d

    tar xf ../$f

    p=$(echo *)
    cd $p
    adjust_git_filter
    cd ..
    tar cf ../$f $p

    cd ..
done

cd "$toplevel"

for f in tests/worktrees/*.tar; do
    cd_fresh_d

    tar xf ../$f

    p=$(echo *)
    cd $p
    adjust_git_filter
    adjust
    cd ..

    tar cf ../$f $p

    cd ..
done

cd "$toplevel"

rm -rf d

echo ok.
